var nodemailer = require('nodemailer');
var Subscriber = require('../models/subscriber');
var MailConfig = require('../models/mailconfig');
var json2csv = require('json2csv');
var moment = require('moment');
var fs = require('fs');

var transporter = nodemailer.createTransport({
  service: 'Gmail',
  //host: 'smtp.gmail.com',
  //port: 465,
  //secure: true, // secure:true for port 465, secure:false for port 587
  auth: {
    user: 'moveramontanha@gmail.com',
    pass: 'Mover1Montanha2'
  }
});

// Subscriber API
module.exports = function(apiRouter){

	var mailConfig = '';

  MailConfig.find()
  .exec(function(err, result){
    if(err)
    {
      return err;
      console.log(err);
    }
    else
    {
      mailConfig = result[0];
      transporter = nodemailer.createTransport({
        service: mailConfig.service,
        host:  mailConfig.host,
        port: mailConfig.port,
        secure: true,
        auth: {
          user: mailConfig.mail,
          pass: mailConfig.password,
        }
      });
    }

  });

	// get all categories
	apiRouter.get('/subscribers', function(req, res){

		Subscriber.find()
	    .exec(function(err, subscribers){
	        if(err)
					{
						res.send(err);
						console.log(err);
					}
	        else
					{
						res.json(subscribers);
					}
	    });
	});

  // temporary endpoint to set all subscribers to active
	/*apiRouter.get('/allSubscribersActive', function(req, res){

		Subscriber.find()
	    .exec(function(err, subscribers){
	        if(err)
					{
						res.send(err);
						console.log(err);
					}
	        else
					{
						for(var i=0; i<subscribers.length; i++)
						{
							//console.log(subscribers[i].active);
							subscribers[i].active = true;
							subscribers[i].save();

								if(i == subscribers.length -1)
									res.send(200);
						}

					}
	    });
	});*/

	// add a subscriber
	apiRouter.post('/subscribers', function(req, res){

		var subscriber = new Subscriber();
		subscriber.email = req.body.email;
		subscriber.dateOfSubscription = Date.now();
    subscriber.active = req.body.active;

		subscriber.save(function(err, subscriber){
			if(err) res.send(err);

			res.json(subscriber);
		})
	});

	// get total number of subscribers
	apiRouter.get('/subscribers/countAll', function(req, res){

    var query = {};

		Subscriber.find().sort('dateOfSubscription').count()
	    .exec(function(err, stats){
					console.log(stats);
	        if(err)
					{
						res.send(err);
						console.log(err);
					}
	        else
					{
						res.json(stats);
					}
	    });
	});

	// get total number of subscribers
	apiRouter.get('/subscribers/countAllActive', function(req, res){

    var query = {};

		Subscriber.find({'active':true}).sort('dateOfSubscription').count()
	    .exec(function(err, stats){
					console.log(stats);
	        if(err)
					{
						res.send(err);
						console.log(err);
					}
	        else
					{
						res.json(stats);
					}
	    });
	});


	// get all subscribers time metrics statistics
	apiRouter.get('/subscribers/timemetrics', function(req, res){

		Subscriber.find().sort('dateOfSubscription')
			.exec(function(err, posts){
					if(err)
					{
						res.send(err);
						console.log(err);
					}
					else
					{
						var averageHours = [];
						var countsHours = [];

						var averageDay = [];
						var countsDay = [];

						var averageFullDate = [];
						var countFullDate = [];

						for(var i=0; i<posts.length; i++)
						{
							var date = new Date(posts[i].dateOfSubscription);
							var hour = date.getHours();
							var day = date.getDay();
							var fullDateNoTime = new Date(date.getFullYear(),date.getMonth(),date.getDate()).toString();

							if(averageHours.indexOf(hour) > -1)
							{
								countsHours[averageHours.indexOf(hour)] = countsHours[averageHours.indexOf(hour)]+1;
							}
							else {
								averageHours.push(hour);
								countsHours.push(1);
							}

							if(averageDay.indexOf(day) > -1)
							{
								countsDay[averageDay.indexOf(day)] = countsDay[averageDay.indexOf(day)]+1;
							}
							else {
								averageDay.push(day);
								countsDay.push(1);
							}

							if(averageFullDate.indexOf(fullDateNoTime) > -1)
							{
								countFullDate[averageFullDate.indexOf(fullDateNoTime)] = countFullDate[averageFullDate.indexOf(fullDateNoTime)]+1;
							}
							else {
								averageFullDate.push(fullDateNoTime);
								countFullDate.push(1);
							}
						}

						var maxNrHours = 0;
						var maxNrDay = 0;
						var maxNrFullDate = 0;

						var timeMetrics = {};
						timeMetrics.hour = [];
						timeMetrics.day = [];
						timeMetrics.fulldate = [];

						for(var i=0; i<countsHours.length; i++)
						{
							if(maxNrHours < countsHours[i])
								maxNrHours = countsHours[i];
						}

						for(var i=0; i<countsHours.length; i++)
						{
							if(countsHours[i] == maxNrHours)
								timeMetrics.hour.push(averageHours[i]);
						}

						for(var i=0; i<countsDay.length; i++)
						{
							if(maxNrDay < countsDay[i])
							{
								maxNrDay = countsDay[i];
							}
						}

						for(var i=0; i<countsDay.length; i++)
						{
							if(countsDay[i] == maxNrDay)
								timeMetrics.day.push(averageDay[i]);
						}

						for(var i=0; i<countFullDate.length; i++)
						{
							if(maxNrFullDate < countFullDate[i])
							{
								maxNrFullDate = countFullDate[i];
							}
						}

						for(var i=0; i<countFullDate.length; i++)
						{
							if(countFullDate[i] == maxNrFullDate)
								timeMetrics.fulldate.push(averageFullDate[i]);
						}

						res.json(timeMetrics);
					}

			});
	});


	// get number of subscribers in a given date
	// uso : /subscribers/count?startDate=August-13-2017&endDate=August-18-2017
	apiRouter.get('/subscribers/count', function(req, res){
		var query = {};

		if(req.query.startDate && req.query.endDate)
		{
			var startDateFormat = req.query.startDate.split('-');
			var endDateFormat = req.query.endDate.split('-');

			var start = new Date(startDateFormat[0]+" "+startDateFormat[1]+", "+startDateFormat[2]);
			var end = new Date(endDateFormat[0]+" "+endDateFormat[1]+", "+endDateFormat[2]);

			query.dateOfSubscription = {'$lte': end, '$gte':start};
		}

		console.log(query);

		Subscriber.find(query).sort('dateOfSubscription')//.count()
			.exec(function(err, stats){
					console.log(stats);
					if(err)
					{
						console.log(err);
						res.send(err);
					}
					else
					{
						res.json(stats);
					}
			});
	});

	// get a single subscriber
	apiRouter.get('/subscribers/:id', function(req, res){
		Subscriber.findById(req.params.id, function(err, subscriber){
			if (err) res.send(err);
			res.json(subscriber);
		});
	});

  // get a single subscriber by email
	apiRouter.get('/subscribers/email/:email', function(req, res){
		Subscriber.find({'email':req.params.email}, function(err, subscriber){
			if (err) res.send(err);
			res.json(subscriber);
		});
	});


	// update a subscriber
	apiRouter.put('/subscribers/:id', function(req, res){
		Subscriber.findById(req.params.id, function(err, subscriber){

			if(err) res.send(err);

			subscriber.email = req.body.email;
			subscriber.dateOfSubscription = req.body.dateOfSubscription;
			subscriber.active = req.body.active;

			subscriber.save(function(err){
				if(err) res.send(err);

				res.json({ message: 'Subscriber updated!' });
			})
		});
	});

	// delete a post
	apiRouter.delete('/subscribers/:id', function(req, res){

		Subscriber.findById(req.params.id, function(err, subscriber){
			if (err) res.send(err);

			transporter.sendMail(
	      {
	        from: mailConfig.mail,
	        to: mailConfig.mail,
	        subject: 'Mover-A-Montanha : Desinscrição de Seguidor : ' + subscriber.email,
	        html: "<p>Informamos que o subscritor com o email "+subscriber.email+" desistiu de seguir o blog Mover-A-Montanha.</p><br><p><strong>Este email foi enviado de forma autom&aacute;tica atrav&eacute;s do nosso servidor de email.</strong></p><p>&nbsp;</p>"
	      }
	    );

			Subscriber.remove({
				_id: req.params.id
			}, function(err, subscriber){
				if(err) res.send(err);

				res.json({ message: 'Subscriber deleted!' });
			})
		});
	});

  // delete a post
	apiRouter.get('/unsubscribe/:id', function(req, res){

		Subscriber.findById(req.params.id, function(err, subscriber){
			if (err) res.send(err);

			transporter.sendMail(
	      {
	        from: mailConfig.mail,
	        to: mailConfig.mail,
	        subject: 'Mover-A-Montanha : Desinscrição de Seguidor : ' + subscriber.email,
	        html: "<p>Informamos que o subscritor com o email "+subscriber.email+" desistiu de seguir o blog Mover-A-Montanha.</p><br><p><strong>Este email foi enviado de forma autom&aacute;tica atrav&eacute;s do nosso servidor de email.</strong></p><p>&nbsp;</p>"
	      }
	    );

			subscriber.active = false;

			subscriber.save();

			res.send('<h1 style="text-align:center; margin-top:4vh;">Caro subscritor, temos pena de o ver partir!</h1><h2 style="text-align:center;">Envie-nos uma mensagem atrav&eacute;s do blog para sabermos o que correu mal. Obrigado!</h2>');

		});
	});

	// get subscribers csv
	apiRouter.get('/getSubscribersCSV', function(req, res){
		Subscriber.find()
	    .exec(function(err, subscribers){
	        if(err)
					{
						res.send(err);
						console.log(err);
					}
	        else
					{
						var subscribersToCSV = [];

						for(var i=0; i<subscribers.length; i++)
						{
							var activo = 'S';

							if(subscribers[i].active == false)
								activo = 'N';

							subscribersToCSV.push({'email':subscribers[i].email, 'activo':activo, 'data de subscrição':subscribers[i].dateOfSubscription});

							if(i == subscribers.length-1)
							{
								var filename = 'subscritores-'+ moment().format('YYYY-MMM-D');
								var csv = json2csv({data:subscribersToCSV});
								res.setHeader('Content-disposition', 'attachment; filename='+filename+'.csv');
								res.set('Content-Type', 'text/csv');
								res.status(200).send(csv);
							}
						}
					}
	    });
	});
};
