var nodemailer = require('nodemailer');
var multer = require('multer');
var MailConfig = require('../models/mailconfig');
var Token = require('../models/token');
var User = require('../models/user');

var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'public/uploads/images/')
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + '.jpg') //Appending .jpg
  }
});

var upload = multer({ storage: storage });

var transporter = nodemailer.createTransport({
  service: 'Gmail',
  //host: 'smtp.gmail.com',
  //port: 465,
  //secure: true, // secure:true for port 465, secure:false for port 587
  auth: {
    user: 'moveramontanha@gmail.com',
    pass: 'Mover1Montanha2'
  }
});

// Services API
module.exports = function(apiRouter){

  var mailConfig = '';

  MailConfig.find()
  .exec(function(err, result){
    if(err)
    {
      return err;
      console.log(err);
    }
    else
    {
      mailConfig = result[0];

      transporter = nodemailer.createTransport({
        service: mailConfig.service,
        host:  mailConfig.host,
        port: mailConfig.port,
        secure: true,
        auth: {
          user: mailConfig.mail,
          pass: mailConfig.password,
        }
      });
    }

  });

  // new subscriber email service
  apiRouter.post('/newSubscriberEmail', function(req, res){

    var data = req.body;

    transporter.sendMail(
      {
        from: mailConfig.mail,
        to: mailConfig.mail,
        subject: 'Mover-A-Montanha : Novo Subscritor ' + data.email,
        html: "<p>Informamos que um novo subscritor com o email "+data.email+" come&ccedil;ou a seguir o blog.</p><br><p><strong>Este email foi enviado de forma autom&aacute;tica atrav&eacute;s do nosso servidor de email.</strong></p><p>&nbsp;</p>"
      }, function(err){
        if(err)
        console.log(err);
      });

      res.json(data);
    });

    // unsubscription email
    apiRouter.post('/subscriberDeletedEmail', function(req, res){

      var data = req.body;

      transporter.sendMail(
        {
          from: mailConfig.mail,
          to: mailConfig.mail,
          subject: 'Mover-A-Montanha : Desinscri&ccedil;&atilde;o ' + data.email,
          html: "<p>Informamos que o subscritor com o email "+data.email+" desistiu de seguir o blog.</p><br><p><strong>Este email foi enviado de forma autom&aacute;tica atrav&eacute;s do nosso servidor de email.</strong></p><p>&nbsp;</p>"
        }
      );

      res.json(data);
    });

    // contact form service
    apiRouter.post('/contact-form', function(req, res){

      var data = req.body;

      transporter.sendMail(
        {
          from: mailConfig.mail,
          to: mailConfig.mail,
          subject: 'Mover-A-Montanha : Novo contacto',
          html: "<b><i>Contacto efectuado por "+data.contactName+" com o email - "+data.contactEmail+"</i></b><br>"+ data.contactMessage
        }
      );

      res.json(data);
    });

    apiRouter.post('/pwdRecoverMail', function(req, res) {
      var data = req.body;

      var now = new Date();
      var createDate = new Date('2017-10-20T15:40:52.891Z');

      require('crypto').randomBytes(48, function(ex, buf) {
        var userToken = buf.toString('hex');
        User.findOne({email: (data.email)}, function(err, usr) {
          if(err || !usr) {
            console.log('err');
          }

          var token = new Token();
          token.token = userToken;
          token.email = usr.email;
          token.save(function(err, token){
            transporter.sendMail(
              {
                from: mailConfig.mail,
                to: data.email,
                subject: 'Mover-A-Montanha : Recuperação de Palavra-Passe',
                html: "<p></p> <p style='text-align: center;'><img src='https://www.moveramontanha.pt/home/img/moveramontanha_header.png' style='width: 100%;'/>​</p><h1 style='text-align: center;'>​Mover-A-Montanha</h1><h3 style='text-align: center;'>​<u>Recuperação de Palavra-Passe</u><br/></h3><hr/><p style='text-align:center;'>Por favor clique <a href='https://www.moveramontanha.pt/admin/pwdRecoverChangePwd/"+token.token+"'>aqui</a> para restaurar a sua palavra-passe.</p>"
              }
            );
            res.redirect('/admin/pwdrecoversentmsg');
          });
        });
      });
    });

    apiRouter.post('/pwdchangesystem', function(req, res) {

      var data = req.body;

      Token.findOne({token: (data.token)})
      .exec(function(err, token){
        if(err)
        {
          return err;
          console.log(err);
        }
        else
        {
          var now = new Date();
          var isPWDProcessValid = true;

          if((now - token.createDate) > 1800000)
          {
            res.render('admin/pwdError', { status: 500, message:'O periodo de alteração de palavra-passe expirou. Por favor tente de novo.' });
            isPWDProcessValid = false;
          }

          if(token.email != data.email)
          {
            res.render('admin/pwdError', { status: 500, message:'Este utilizador não existe, ou não corresponde à palavra-passe que deseja alterar. Por favor tente de novo.' });
            isPWDProcessValid = false;
          }

          if(data.newpassword != data.confirmpassword)
          {
            res.render('admin/pwdError', { status: 500, message:'As palavras-passe não são idênticas. Por favor tente de novo.' });
            isPWDProcessValid = false;
          }

          if(isPWDProcessValid) //if token is not expired and user corresponds
          {
            User.findByUsername(token.email).then(function(sanitizedUser){
              if (sanitizedUser){
                sanitizedUser.setPassword(data.newpassword, function(err, results)
                {
                  if(err)
                  {
                    res.render('admin/pwdError', { status: 500, message: err });
                  }
                  else {
                    sanitizedUser.save();
                    res.render('admin/pwdError', { status: 200, message:'Palavra-passe alterada com sucesso para o utilizador '+token.email+'.'  });
                  }

                });

              }
            },function(err){
              res.render('admin/pwdError', { status: 500, message: err });
            });
            /*sanitizedUser.setPassword(data.newPassword, function(err){
            if(err)
            {
            res.render('admin/pwdError', { status: 500, message: err });
          }
          else {
          sanitizedUser.save(function(err, user)
          {
          if(err)
          {
          res.render('admin/pwdError', { status: 500, message: err });
        }
        else {
        res.render('admin/pwdError', { status: 200, message:'Palavra-passe alterada com sucesso para o utilizador '+token.email+'.'  });
      }
    });
  }

});*/
} else {
  res.render('admin/pwdError', { status: 500, message:'Ocorreu um erro ao alterar a palavra-passe.' });
}
/*},function(err){
console.error(err);
res.render('admin/pwdError', { status: 500, message:'ERRO desconhecido. Por favor tente de novo e/ou contacte os administradores.' });
});*/
/*}
else {
res.render('admin/pwdError', { status: 500, message:'Ocorreu um problema. Por favor tente de novo e/ou contacte os administradores.' });
}*/
}

});
});

// add an author
apiRouter.post('/imageUpload' ,upload.single('uploadImageFile'), function(req, res){
  res.json({ path: req.file.path });
});

};
